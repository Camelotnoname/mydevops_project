Чтобы развернуть инфраструктуру проекта, сначала необходимо установить terraform, подробную инструкцию установки необходимо взять
с официального сайта terraform, по ссылке ниже:

https://www.terraform.io/docs/cli/install/apt.html

Инфраструктура будет развернута у облачного провайдера AWS, для того чтобы terraform мог взаимодействовать с провайдером
необходимо установить клиент AWS, инструкция приведена ниже:

https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html

Также необходимо, чтобы на вашем компьютере были установлены, такие иснтрументы,
как python3, pip3, boto3.

Скачать Python можно с официального сайта https://www.python.org/downloads/
После установки Python и настройки Path, устанавливаем pip3, коммандой:
```shell
   sudo apt install python3-pip
   ```
После заверешения установки pip3, устанавливаем пакет интерграции boto3:
```shell
   sudo pip3 install boto3
   ```

### Создаем инфраструктуру с помощью шагов приведенных ниже: ###
1. Перейти в директорию /Infra/terraform/test или prod, в зависимости какое окружение необходимо развернуть,
   вместе с окружением test, разворачивается инстанс для мониторинга:
2. Команда для настройки провайдера:
   ```shell
   terraform init
   ```
3. Команда для просмотра вносимых изменений, а также проверки синтаксиса:
   ```shell
   terraform plan
   ```
4. Команда для создания инфраструктуры:
   ```shell
   terraform apply -auto-approve
   ```

### Завершение работы с инфраструктурой ###
1. Перейти в директорию /Infra/terraform/test или prod
2. Выполнить комманду
   ```shell
   terraform destroy --auto-approve
   ```

### Для установки и настройки инфраструктуры для работы с Docker, необходимо зайти в каталог /Infra/ansible: ###

1. Для того, чтобы установить и настроить вебсервера необходимо запустить плейбук, выполняем ниже приведенную команду:
   ```shell
   ansible-playbook -i aws_ec2.yaml -b webservers.yml
   ```
2. После этого необходимо запустить плейбук для настройки мониторинга, командой:
   ```shell
   ansible-playbook -i aws_ec2.yaml -b monitoring.yml
   ```


### Для того чтобы работать с мониторингом ###
1. Необходимо в вашем браузере в адресной строке набрать:
   ```shell
      - для вывода метрик через Prometheus, набрать http://monitoring.zhupinskiy.click
      - для вывода метрик через Grafana, набрать http://grafana.zhupinskiy.click, логин на вход: admin, пароль: mypassword
   ```

   
### Runner необходим для доставки нашего кода на наш сервер. Для того, чтобы развернуть Gitlab-Runner для нашего проекта: ###

1. Переходим в директорию с инфраструктурой для раннера, /Infra/Gitlab_runner/terraform и выполняем шаги приведенные в разделе создание инфраструктуры.
2. После создания инстанса для раннера, необходимо установить сам раннер и настроить его для работы с нашим облачным Gitlabом, переходим в каталог /Infra/Gitlab_runner/ansible, 
   и выполняем команду: 
   ```shell
   ansible-playbook -i aws_ec2.yaml -b runner.yml
   ```
3. Также в нашем облачном Gitlabe необходимо указать IP ADDRESS нашего вебсервера, заходим на сайт https://gitlab.com, переходим в рабочий каталог нашего проекта,
   затем Settings > CI/CD > Variables > редактируем запись DOCKER_HOST_IP в поле Value указываем наш IP адрес.

