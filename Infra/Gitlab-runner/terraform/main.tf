provider "aws" {
  region = "us-east-1"
}


resource "aws_eip" "my_static_ip" {
  instance = aws_instance.my_runner.id
  tags = {
    Name  = "Runner Server IP"
  }
}

# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Запускаем инстанс
resource "aws_instance" "my_runner" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.my_runner.id]
  key_name = "MacBook"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Runner"                  
  }

  lifecycle {
    create_before_destroy = true
  }

}


resource "aws_security_group" "my_runner" {
  name        = "Runner Server Security Group"
  description = "Security group for accessing traffic to our Runner Server"


  dynamic "ingress" {
    for_each = ["80", "22", "9100", "9080", "9096"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Runner Server SecurityGroup"
  }
}

# Выведем IP адрес сервера
output "my_runner_ip" {
  description = "Elatic IP address assigned to our WebSite"
  value       = aws_eip.my_static_ip.public_ip
}