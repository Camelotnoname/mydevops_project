# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  region = "us-east-1"
}

#Указываем модуль, который будем использовать
module "webserver-cluster" {
    source ="/Users/Camelot/DevOps/Project/Infra/terraform/modules/webserver-cluster"

#Указываем название и путь где хранится наше состояние terraform
    cluster_name           = "test"
    db_remote_state_bucket = "terraform-state-camelotnoname"
    db_remote_state_key    = "global/s3/terraform.tfstate"

}

#Указываем модуль, который будем использовать

module "monitoring" {
    source ="/Users/Camelot/DevOps/Project/Infra/terraform/modules/monitoring"

}