terraform {
  backend "s3" {
    bucket = "terraform-state-camelotnoname"
    key    = "global/s3/terraform.tfstate"
    region = "us-east-1"
  }
}