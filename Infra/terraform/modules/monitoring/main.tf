# Узнаём, какие есть Дата центры в выбранном регионе
data "aws_availability_zones" "available" {}


# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "monitoring" {
  name = "Monitoring Security Group"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = ["22", "8080", "80", "9090", "3100", "9100", "9000"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"      
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Monitoring"
  }
}

# Запускаем инстанс
resource "aws_instance" "monitoring" {
  # с выбранным образом
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы)
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.monitoring.id]
  key_name = "MacBook"
  user_data       = file("${path.module}/user_data.sh")
  tags = {
    Name  = "Monitoring"
    Tier = "Backend"
    CM = "Grafana"
  }

  lifecycle {
    create_before_destroy = true
  }

}

#Создаем DNS-записи для мониторинкга
resource "aws_route53_record" "monitoring" {
  zone_id = var.dns_zone_id
  name = "monitoring"
  type = "A"
  ttl = "300"
  records = [aws_eip.my_static_ip.public_ip]
}

resource "aws_route53_record" "grafana" {
  zone_id = var.dns_zone_id
  name = "grafana"
  type = "CNAME"
  ttl = "5"
  records = ["monitoring.zhupinskiy.click"]
}

resource "aws_eip" "my_static_ip" {
  instance = aws_instance.monitoring.id
  tags = {
    Name  = "Monitoring Server IP"
  }
}