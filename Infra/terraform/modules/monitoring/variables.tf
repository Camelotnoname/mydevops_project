variable "dns_zone_id" {
    default = "Z006564114C8OK6VQIFAK"
}

variable "ns_servers" {
    type = list(string)
    default = ["ns-349.awsdns-43.com", "ns-1248.awsdns-28.org", "ns-1726.awsdns-23.co.uk", "ns-684.awsdns-21.net"]
}

