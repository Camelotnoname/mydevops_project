# Выведем IP адрес сервера
output "my_monitoring_site_ip" {
  description = "Elatic IP address assigned to our Docker Server"
  value       = aws_eip.my_static_ip.public_ip
}
