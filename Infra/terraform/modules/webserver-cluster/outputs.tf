# Выведем в консоль DNS имя нашего сервера
output "web_loadbalancer_url" {
  value = aws_elb.web.dns_name
}

# Выведем IP адрес сервера
#output "my_docker_site_ip" {
#  description = "Elatic IP address assigned to our Docker Server"
#  value       = aws_eip.my_static_ip.public_ip
#}
