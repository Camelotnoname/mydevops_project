variable "dns_zone_id" {
    default = "Z006564114C8OK6VQIFAK"
}

variable "ns_servers" {
    type = list(string)
    default = ["ns-349.awsdns-43.com", "ns-1248.awsdns-28.org", "ns-1726.awsdns-23.co.uk", "ns-684.awsdns-21.net"]
}


variable "cluster_name" {
    description = "The name to use all the cluster resources"
    type        = string
}

variable "db_remote_state_bucket" {
    description = "The name of the S3 bucket for the database's remote state"
    type        = string
}

variable "db_remote_state_key" {
    description = "The path for the database's remote state in S3"
    type        = string
}


